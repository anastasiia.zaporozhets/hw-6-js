function createNewUser() {
    const firstName = prompt("Введіть ваше імя");
    const lastName = prompt("Введіть ваше прізвище");
    const birthdayStr = prompt("Введіть вашу дату народження у форматі dd.mm.yyyy");
    const [day, month, year] = birthdayStr.split(".");
    const birthday = new Date(`${year}-${month}-${day}`);
    const newUser = {
        firstName: firstName,
        lastName: lastName,
        birthday: birthday,
        getLogin: function() {
            return this.firstName[0].toLowerCase() + this.lastName.toLowerCase();
        },
        getAge: function (){
            let today = new Date();
            let age = today.getFullYear() - this.birthday.getFullYear();
            let month = today.getMonth() - this.birthday.getMonth();
            if (month < 0 || (month === 0 && today.getDate() < this.birthday.getDate())) {
                age--;
            }
            return age;
        },
        getPassword: function() {
            let password = this.firstName.charAt(0).toUpperCase() + this.lastName.toLowerCase() + this.birthday.getFullYear();
            return password;
        }

    };
    console.log(newUser);
    console.log("Логін " + newUser.getLogin());
    console.log("Вік " + newUser.getAge());
    console.log("Пароль " + newUser.getPassword());
}

createNewUser();
